
package Modelos;

import Persistencia.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ADMIN
 */
public class INVENTARIOS {
    // Atributos
    private int      ID_CODIGO;
    private String   REFERENCIA;
    private String   DESCRIPCION;
    private String   UNIDAD;
    private int   MINIMO;
    private int   MAXIMO;
    private int   EXISTENCIA;
    private int   COSTO;
    private int   PRECIO;

    public INVENTARIOS(int ID_CODIGO, String REFERENCIA, String DESCRIPCION, String UNIDAD, int MINIMO, int MAXIMO, int EXISTENCIA, int COSTO, int PRECIO) {
        this.ID_CODIGO = ID_CODIGO;
        this.REFERENCIA = REFERENCIA;
        this.DESCRIPCION = DESCRIPCION;
        this.UNIDAD = UNIDAD;
        this.MINIMO = MINIMO;
        this.MAXIMO = MAXIMO;
        this.EXISTENCIA = EXISTENCIA;
        this.COSTO = COSTO;
        this.PRECIO = PRECIO;
    }
    
    public INVENTARIOS(){}

    public int getID_CODIGO() {
        return ID_CODIGO;
    }

    public void setID_CODIGO(int ID_CODIGO) {
        this.ID_CODIGO = ID_CODIGO;
    }

    public String getREFERENCIA() {
        return REFERENCIA;
    }

    public void setREFERENCIA(String REFERENCIA) {
        this.REFERENCIA = REFERENCIA;
    }

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }

    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }

    public String getUNIDAD() {
        return UNIDAD;
    }

    public void setUNIDAD(String UNIDAD) {
        this.UNIDAD = UNIDAD;
    }

    public int getMINIMO() {
        return MINIMO;
    }

    public void setMINIMO(int MINIMO) {
        this.MINIMO = MINIMO;
    }

    public int getMAXIMO() {
        return MAXIMO;
    }

    public void setMAXIMO(int MAXIMO) {
        this.MAXIMO = MAXIMO;
    }

    public int getEXISTENCIA() {
        return EXISTENCIA;
    }

    public void setEXISTENCIA(int EXISTENCIA) {
        this.EXISTENCIA = EXISTENCIA;
    }

    public int getCOSTO() {
        return COSTO;
    }

    public void setCOSTO(int COSTO) {
        this.COSTO = COSTO;
    }

    public int getPRECIO() {
        return PRECIO;
    }

    public void setPRECIO(int PRECIO) {
        this.PRECIO = PRECIO;
    }
    
    // Guardar
    public void guardar() throws ClassNotFoundException, SQLException{
        //CRUD - C
        Conexion con = new Conexion(); //instancia
        
        String  sql = "INSERT INTO INVENTARIOS(ID_CODIGO, REFERENCIA, DESCRIPCION, UNIDAD, MINIMO, MAXIMO, EXISTENCIA, COSTO, PRECIO) VALUES("+getID_CODIGO()+",'"+getREFERENCIA()+"','"+getDESCRIPCION()+"','"+getUNIDAD()+"',"+getMINIMO()+","+getMAXIMO()+","+getEXISTENCIA()+","+getCOSTO()+","+getPRECIO()+");" ;
              
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        
        statement.execute(sql);
        conex.close();
    }
    ///Consultar

    public void consultar() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        ResultSet rs;
        int C=0;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM INVENTARIOS";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        
        
        System.out.println("************************************************************************");
        System.out.println("***                 Información de Productos  Registrados              ***");
        System.out.println("************************************************************************");
        while(rs.next()){
            C=C+1;
            System.out.println("****Registro Número..... "+C+"*********");
            System.out.println("Codigo Producto..... "+rs.getInt(1)); 
            System.out.println("Referencia.......... "+rs.getString(2));
            System.out.println("Descripción......... "+rs.getString(3));
            System.out.println("Unidad.............. "+rs.getString(4));
            System.out.println("Precio Producto..... "+rs.getInt(5));
            System.out.println("Costo Producto...... "+rs.getInt(6));
            System.out.println("Existencia Actual... "+rs.getInt(7));
            System.out.println("Minimo Existencia... "+rs.getInt(8));
            System.out.println("Maximo Existencia... "+rs.getInt(9));
            }
        System.out.println("************************************************************************");
        
        conex.close();
        
    }
    
  // Actualizar
    
    public void actualizar() throws ClassNotFoundException, SQLException{
        // CRUD -U
        Conexion con = new Conexion();
        
        String sql = "UPDATE INVENTARIOS SET  REFERENCIA = '"+getREFERENCIA()+"',DESCRIPCION='"+ getDESCRIPCION()+"',UNIDAD='"+getUNIDAD()+"',MINIMO="+getMINIMO()+",MAXIMO="+getMAXIMO()+",EXISTENCIA="+getEXISTENCIA()+",COSTO="+getCOSTO()+",PRECIO="+getPRECIO()+" WHERE ID_CODIGO ="+getID_CODIGO()+";";
          System.out.println(sql);
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
        conex.close();
    
        
    }
    // Borrar
   
    public void borrar() throws SQLException, ClassNotFoundException{
        //CRUD -D74
        Conexion con = new Conexion();
        String sql = "DELETE FROM INVENTARIOS WHERE ID_CODIGO ="+getID_CODIGO()+";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
        conex.close();
    
    }
   //comprobar cantidad de registro
    public int comprobar() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        int p=0;
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM INVENTARIOS";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        if(rs.next()){
           p=1;
           }
        conex.close();
        return p;
    }
   
/*REALIZAR UNA CONSULTA DETALLA DE UN REGISTRO*/
   public void consultar_det() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM INVENTARIOS WHERE ID_CODIGO="+getID_CODIGO();
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        
        
        System.out.println("************************************************************************");
        System.out.println("***                Información del Producto Registrado               ***");
        System.out.println("************************************************************************");
        while(rs.next()){
            System.out.println("Codigo Producto..... "+rs.getInt(1)); 
            System.out.println("Referencia.......... "+rs.getString(2));
            System.out.println("Descripción......... "+rs.getString(3));
            System.out.println("Unidad.............. "+rs.getString(4));
            System.out.println("Precio Producto..... "+rs.getInt(5));
            System.out.println("Costo Producto...... "+rs.getInt(6));
            System.out.println("Existencia Actual... "+rs.getInt(7));
            System.out.println("Minimo Existencia... "+rs.getInt(8));
            System.out.println("Maximo Existencia... "+rs.getInt(9));
           }
        System.out.println("************************************************************************");
        
        conex.close();
        
    }
   
   
   public int comprobar_det() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        int p=0;
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM INVENTARIOS WHERE ID_CODIGO="+getID_CODIGO();
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        if(rs.next()){
            p=1;
           }
        conex.close();
        return p;
   }
   /*metodo para verificar que no existen registros en la tabla totales */
      public int verificar() throws ClassNotFoundException, SQLException{
        return 0;
        //CRUD -R 
       /* int p=1;
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM TOTALES WHERE ID_TERCEROS="+getID_TERCERO();
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        if(rs.next()){
           p=1;
           }
        conex.close();
        return p;
   }
    */    
      }  
    
}

