    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;


import Persistencia.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ADMIN
 */
public class DATOS {
       
    // Atributos
    private String   ID;
    private String   NOM_TIENDA;
    private String   NUM_IDENTI;
    private String   NOM_REPRE;
    private String   TELEFONO;

    public DATOS(String ID, String NOM_TIENDA, String NUM_IDENTI, String NOM_REPRE, String TELEDONO) {
        this.ID = ID;
        this.NOM_TIENDA = NOM_TIENDA;
        this.NUM_IDENTI = NUM_IDENTI;
        this.NOM_REPRE = NOM_REPRE;
        this.TELEFONO = TELEFONO;
    }

    public DATOS() {
           }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getNOM_TIENDA() {
        return NOM_TIENDA;
    }

    public void setNOM_TIENDA(String NOM_TIENDA) {
        this.NOM_TIENDA = NOM_TIENDA;
    }

    public String getNUM_IDENTI() {
        return NUM_IDENTI;
    }

    public void setNUM_IDENTI(String NUM_IDENTI) {
        this.NUM_IDENTI = NUM_IDENTI;
    }

    public String getNOM_REPRE() {
        return NOM_REPRE;
    }

    public void setNOM_REPRE(String NOM_REPRE) {
        this.NOM_REPRE = NOM_REPRE;
    }

    public String getTELEFONO() {
        return TELEFONO;
    }

    public void setTELEFONO(String TELEFONO) {
        this.TELEFONO = TELEFONO;
    }

   
    
    
    // Guardar
    public void guardar() throws ClassNotFoundException, SQLException{
        //CRUD - C
        Conexion con = new Conexion(); //instancia
        String  sql = "INSERT INTO DATOS(ID, NOM_TIENDA, NUM_IDENTI, NOM_REPRE, TELEFONO) VALUES('"+getID()+"','"+getNOM_TIENDA()+"','"+getNUM_IDENTI()+"','"+getNOM_REPRE()+"','"+getTELEFONO()+"');" ;
        
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        
        statement.execute(sql);
        conex.close();
    }

    /*REALIZAR UNA CONSULTA DETALLA DE UN REGISTRO*/
   public void consultar_det() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM DATOS WHERE ID="+getID();
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        
        
        System.out.println("************************************************************************");
        System.out.println("***                Información Basica de la Tienda                   ***");
        System.out.println("************************************************************************");
        while(rs.next()){
            System.out.println("Identificación............... "+rs.getString(1)); 
            System.out.println("Nombre de la tienda.......... "+rs.getString(2));
            System.out.println("Identificación Representante. "+rs.getString(3));
            System.out.println("Nombre del Representante..... "+rs.getString(4));
            System.out.println("Telefonos de la Tienda....... "+rs.getString(5));
           }
        System.out.println("************************************************************************");
        
        conex.close();
        
    }
    
    // Consultar
    public void consultar() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM DATOS";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        
        
        System.out.println("************************************************************************");
        System.out.println("***                 Información Basica de la Tienda                  ***");
        System.out.println("************************************************************************");
        while(rs.next()){
            System.out.println("Identificación............... "+rs.getString(1)); 
            System.out.println("Nombre de la tienda.......... "+rs.getString(2));
            System.out.println("Identificación Representante. "+rs.getString(3));
            System.out.println("Nombre del Representante..... "+rs.getString(4));
            System.out.println("Telefonos de la Tienda....... "+rs.getString(5));
           }
        System.out.println("************************************************************************");
        
        conex.close();
        
    }
    
    
    
    // Actualizar
    
    public void actualizar() throws ClassNotFoundException, SQLException{
        // CRUD -U
        Conexion con = new Conexion();
        
        String sql = "UPDATE DATOS SET  NOM_TIENDA = '"+getNOM_TIENDA()+"', NUM_IDENTI = '"+getNUM_IDENTI()+"',NOM_REPRE='"+ getNOM_REPRE()+"',TELEFONO='"+getTELEFONO()+"' WHERE ID ='"+getID()+"';";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
        conex.close();
    
    }
    
   
    // Borrar
   
    public void borrar() throws SQLException, ClassNotFoundException{
        //CRUD -D74
        Conexion con = new Conexion();
        String sql = "DELETE FROM DATOS WHERE ID ="+getID()+";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
        conex.close();
    
    }
    
    
    //comprobar cantidad de registro
    public int comprobar() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        int p=0;
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM datos";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        if(rs.next()){
           p=1;
           }
        conex.close();
        return p;
    }
    
       public int comprobar_det() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        int p=0;
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM DATOS WHERE ID="+getID();
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        if(rs.next()){
            p=1;
           }
        conex.close();
        return p;
   }
    
    public String obtener_id() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        int p=0;
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM datos",ID;
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        rs.next();
        ID=rs.getString(1);
        conex.close();
        return ID;
    }
    // Sobre escribir el metodo ToString para ver los valor de los atributos de una clase

   
    // Creamos el 

    @Override
    public String toString() {
        return "DATOS{" + "ID=" + ID + ", NOM_TIENDA=" + NOM_TIENDA + ", NUM_IDENTI=" + NUM_IDENTI + ", NOM_REPRE=" + NOM_REPRE + ", TELEFONO=" + TELEFONO + '}';
    }

    
    
}

    


