/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;


import Persistencia.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ADMIN
 */
public class TERCEROS {
       
    // Atributos
    private int      ID_TERCERO;
    private String   NUMERO;
    private String   NOMBRE;
    private String   APELLIDO;
    private String   TELEFONO;
    private String   DIRECCION;
    private String   EMAIL;
    private String   CIUDAD;
    private String   TIPO_PROV;
    private String   TIPO_CLI;
    private String   TIPO_USER;
    private int   PERM_USER;

    public TERCEROS(int ID_TERCERO, String NUMERO, String NOMBRE, String APELLIDO, String TELEFONO, String DIRECCION, String EMAIL, String CIUDAD, String TIPO_PROV, String TIPO_CLI, String TIPO_USER, int PERM_USER) {
        this.ID_TERCERO = ID_TERCERO;
        this.NUMERO = NUMERO;
        this.NOMBRE = NOMBRE;
        this.APELLIDO = APELLIDO;
        this.TELEFONO = TELEFONO;
        this.DIRECCION = DIRECCION;
        this.EMAIL = EMAIL;
        this.CIUDAD = CIUDAD;
        this.TIPO_PROV = TIPO_PROV;
        this.TIPO_CLI = TIPO_CLI;
        this.TIPO_USER = TIPO_USER;
        this.PERM_USER = PERM_USER;
    }

    public TERCEROS(){}

    public int getID_TERCERO() {
        return ID_TERCERO;
    }

    public void setID_TERCERO(int ID_TERCERO) {
        this.ID_TERCERO = ID_TERCERO;
    }

    public String getNUMERO() {
        return NUMERO;
    }

    public void setNUMERO(String NUMERO) {
        this.NUMERO = NUMERO;
    }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    public String getAPELLIDO() {
        return APELLIDO;
    }

    public void setAPELLIDO(String APELLIDO) {
        this.APELLIDO = APELLIDO;
    }

    public String getTELEFONO() {
        return TELEFONO;
    }

    public void setTELEFONO(String TELEFONO) {
        this.TELEFONO = TELEFONO;
    }

    public String getDIRECCION() {
        return DIRECCION;
    }

    public void setDIRECCION(String DIRECCION) {
        this.DIRECCION = DIRECCION;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getCIUDAD() {
        return CIUDAD;
    }

    public void setCIUDAD(String CIUDAD) {
        this.CIUDAD = CIUDAD;
    }

    public String getTIPO_PROV() {
        return TIPO_PROV;
    }

    public void setTIPO_PROV(String TIPO_PROV) {
        this.TIPO_PROV = TIPO_PROV;
    }

    public String getTIPO_CLI() {
        return TIPO_CLI;
    }

    public void setTIPO_CLI(String TIPO_CLI) {
        this.TIPO_CLI = TIPO_CLI;
    }

    public String getTIPO_USER() {
        return TIPO_USER;
    }

    public void setTIPO_USER(String TIPO_USER) {
        this.TIPO_USER = TIPO_USER;
    }

    public int getPERM_USER() {
        return PERM_USER;
    }

    public void setPERM_USER(int PERM_USER) {
        this.PERM_USER = PERM_USER;
    }
    
    

    // Guardar
    public void guardar() throws ClassNotFoundException, SQLException{
        //CRUD - C
        Conexion con = new Conexion(); //instancia
        
        String  sql = "INSERT INTO TERCEROS(ID_TERCERO, NUMERO, NOMBRE, APELLIDO, TELEFONO, DIRECCION, EMAIL, CIUDAD, TIPO_PROV, TIPO_CLI, TIPO_USER,  PERM_USER) VALUES("+getID_TERCERO()+",'"+getNUMERO()+"','"+getNOMBRE()+"','"+getAPELLIDO()+"','"+getTELEFONO()+"','"+getDIRECCION()+"','"+getEMAIL()+"','"+getCIUDAD()+"','"+getTIPO_PROV()+"','"+getTIPO_CLI()+"','"+getTIPO_USER()+"','"+getPERM_USER()+"');" ;
              
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        
        statement.execute(sql);
        conex.close();
    }
    
    // Consultar
    public void consultar() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        ResultSet rs;
        int C=0;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM TERCEROS";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        
        
        System.out.println("************************************************************************");
        System.out.println("***                 Información de Terceros Redistrados              ***");
        System.out.println("************************************************************************");
        while(rs.next()){
            C=C+1;
            System.out.println("****Registro Número..... "+C+"**************************************");
            System.out.println("Codigo Tercero.... "+rs.getInt(1)); 
            System.out.println("Identificación.... "+rs.getString(2));
            System.out.println("Nombre............ "+rs.getString(3));
            System.out.println("Apellido.......... "+rs.getString(4));
            System.out.println("Teléfonos......... "+rs.getString(5));
            System.out.println("Dirección......... "+rs.getString(6));
            System.out.println("Correo Electrónico."+rs.getString(7));
            System.out.println("Ciudad............ "+rs.getString(8));
            System.out.println("Es proveedor[S/N]. "+rs.getString(9));
            System.out.println("Es Cliente [S/N].. "+rs.getString(10));
            System.out.println("Es Usuario [S/N].. "+rs.getString(11));
            System.out.println("Nivel Autorización."+rs.getString(12));
           }
        System.out.println("************************************************************************");
        
        conex.close();
        
    }
    
    // Actualizar
    
    public void actualizar() throws ClassNotFoundException, SQLException{
        // CRUD -U
        Conexion con = new Conexion();
        
        String sql = "UPDATE TERCEROS SET  NUMERO = '"+getNUMERO()+"',NOMBRE='"+ getNOMBRE()+"',APELLIDO='"+getAPELLIDO()+"',TELEFONO='"+getTELEFONO()+"',DIRECCION='"+getDIRECCION()+"',EMAIL='"+getEMAIL()+"',CIUDAD='"+getCIUDAD()+"',TIPO_PROV='"+getTIPO_PROV()+"',TIPO_CLI='"+getTIPO_CLI()+"',TIPO_USER='"+getTIPO_USER()+"',PERM_USER='"+getPERM_USER()+"' WHERE ID_TERCERO ="+getID_TERCERO()+";";
          
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
        conex.close();
    
        
    }
    
   
    // Borrar
   
    public void borrar() throws SQLException, ClassNotFoundException{
        //CRUD -D74
        Conexion con = new Conexion();
        String sql = "DELETE FROM TERCEROS WHERE ID_TERCERO ="+getID_TERCERO()+";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
        conex.close();
    
    }
    
    
    //comprobar cantidad de registro
    public int comprobar() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        int p=0;
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM TERCEROS";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        if(rs.next()){
           p=1;
           }
        conex.close();
        return p;
    }
/*RETORNA EL ID DE UN RESTRIDO*/    
    public int obtener_id() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        int p=0,ID;
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM TERCEROS";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        rs.next();
        ID=rs.getInt(1);
        conex.close();
        return ID;
    }
   
/*REALIZAR UNA CONSULTA DETALLA DE UN REGISTRO*/
   public void consultar_det() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM TERCEROS WHERE ID_TERCERO="+getID_TERCERO();
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        
        
        System.out.println("************************************************************************");
        System.out.println("***                  Información de Tercero Registrado               ***");
        System.out.println("************************************************************************");
        while(rs.next()){
            System.out.println("Codigo Terero..... "+rs.getInt(1)); 
            System.out.println("Identificación.... "+rs.getString(2));
            System.out.println("Nombre............ "+rs.getString(3));
            System.out.println("Apellido.......... "+rs.getString(4));
            System.out.println("Teléfonos......... "+rs.getString(5));
            System.out.println("Dirección......... "+rs.getString(6));
            System.out.println("Correo Electrónico."+rs.getString(7));
            System.out.println("Ciudad............ "+rs.getString(8));
            System.out.println("Es proveedor[S/N]. "+rs.getString(9));
            System.out.println("Es Cliente [S/N].. "+rs.getString(10));
            System.out.println("Es Usuario [S/N].. "+rs.getString(11));
            System.out.println("Nivel Autorización."+rs.getInt(12));
           }
        System.out.println("************************************************************************");
        
        conex.close();
        
    }
   
   
   public int comprobar_det() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        int p=0;
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM TERCEROS WHERE ID_TERCERO="+getID_TERCERO();
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        if(rs.next()){
           p=1;
           }
        conex.close();
        return p;
   }
   /*metodo para verificar que no existen registros en la tabla totales */
      public int verificar() throws ClassNotFoundException, SQLException{
        return 0;
        //CRUD -R 
       /* int p=1;
        ResultSet rs;
        Conexion con = new Conexion();
        String  sql = "SELECT * FROM TOTALES WHERE ID_TERCEROS="+getID_TERCERO();
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        rs = statement.executeQuery (sql);
        if(rs.next()){
           p=1;
           }
        conex.close();
        return p;
   }
    */    
      } 
}
      
