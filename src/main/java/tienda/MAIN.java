
package tienda;

import Modelos.DATOS;
import Modelos.INVENTARIOS;
import Modelos.TERCEROS;
import java.sql.SQLException;
import java.util.Scanner;



public class MAIN {
    public static void main(String[] args) throws SQLException, ClassNotFoundException
    {
        DATOS a = new DATOS();
        TERCEROS C =new TERCEROS();
        INVENTARIOS P=new INVENTARIOS();
      /*PRODUCTOS P=new PRODUCTOS();
        TOTALES  M =new TOTALES();
        MOVIMIENTOS D=new MOVIMIENTOS();
        */
        Scanner T = new Scanner (System.in);
        
                ;
        int M1=0,M2=0,SW;
       while(M1!=7){
            M2=0;
            System.out.println("********************************************************");
            System.out.println("***          Bienvenido al Programa Tendero          ***");
            System.out.println("********************************************************");
            System.out.println("*****            Menu de Operaciones               ******");
            System.out.println("********************************************************");
            System.out.println("*              1.Ingresos                               *");
            System.out.println("*              2.Egresos                                *");
            System.out.println("*              3.Actualizar Productos                  *");
            System.out.println("*              4.Actualizar Clientes                   *");
            System.out.println("*              5.Reportes Especiales                   *");
            System.out.println("*              6.Actualizar Datos Básicos              *");
            System.out.println("*              7.Terminar                              *");
            System.out.println("********************************************************");
            System.out.println("        Ingrese un Número del [1] al [7]  "); 
            M1 = T.nextInt();
            System.out.println(" ");
            switch(M1)
                    {
                   case 1 :    
                        System.out.println("********************************************************");
                        System.out.println("*****     Opción de Ingresos en [CONSTRUCCION]..     ******");
                        System.out.println("********************************************************");                        
                        System.out.println(" Pulse [0] y [ENTER] para continuar..... "); 
                        M2 = T.nextInt();
                        break;

                   case 2:
                         System.out.println("********************************************************");
                         System.out.println("*****     Opción de Egresos en [CONSTRUCCION]..     ******");
                         System.out.println("********************************************************");                        
                         System.out.println(" Pulse [0] y [ENTER] para continuar..... "); 
                         M2 = T.nextInt();
                         break;                         
                   case 3:
/****INICIO PARA EL CRUD  TABLA INVENTARIOS**********************************************************/    
                       while(M2!=5){    
                        System.out.println("\n\n\n\n\n\n\n\n\n");
                        System.out.println("********************************************************");
                        System.out.println("***          Bienvenido al Programa Tendero          ***");
                        System.out.println("********************************************************");
                        System.out.println("******    Menu de Actualización de Productos      ******");
                        System.out.println("********************************************************");
                        System.out.println("*              1.Adicionar                             *");
                        System.out.println("*              2.Consultar                             *");
                        System.out.println("*              3.Modificar                             *");
                        System.out.println("*              4.Eliminar                              *");
                        System.out.println("*              5.Regresar al menu anterior             *");
                        System.out.println("********************************************************");
                        System.out.println("        Ingrese un Número del [1] al [5]  ");
                        M2 = T.nextInt();
                        System.out.println(" ");
                        switch(M2)
                            {
                            case 1 :
                                System.out.println("\n\n\n\n\n\n\n\n\n");
                                System.out.println("********************************************************");
                                System.out.println("*****       Menu  *ADICIONAR* para Produtos       ******");
                                System.out.println("********************************************************");                                                                
                                System.out.println("Codigo Producto.. "); 
                                T.nextLine();
                                P.setID_CODIGO(T.nextInt());
                                T.nextLine();
                                if(P.comprobar_det()==1){
                                    System.out.println("");
                                    System.out.println("********************************************************");
                                    System.out.println("*           !..[ERROR] Registro ya existe              *");
                                    System.out.println("*             Debe Usar otro Código de producto        *");
                                    System.out.println("********************************************************");
                                    System.out.println("");
                                    System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                    SW = T.nextInt();   
                                }
                                else {
                                System.out.println("Referencia....... ");
                                P.setREFERENCIA(T.nextLine()); 
                                System.out.println("Descripcion...... ");
                                P.setDESCRIPCION(T.nextLine());
                                System.out.println("Unidad........... ");
                                P.setUNIDAD(T.nextLine());
                                System.out.println("Precio Venta..... ");
                                P.setPRECIO(T.nextInt());
                                T.nextLine();
                                System.out.println("Costo............ ");
                                P.setCOSTO(T.nextInt());
                                T.nextLine();
                                System.out.println("Existencia Actual.");
                                P.setEXISTENCIA(T.nextInt());
                                T.nextLine();
                                System.out.println("Minimo........... ");
                                P.setMINIMO(T.nextInt());
                                T.nextLine();
                                System.out.println("Maximo........... ");
                                P.setMAXIMO(T.nextInt());
                                T.nextLine();
                                P.guardar();
                                }
                                break;
                            case 2 :
                                System.out.println("********************************************************");
                                System.out.println("*****       Menu  *CONSULTAR* para Productos       *****");
                                System.out.println("********************************************************");
                                System.out.println("\n\n\n\n\n\n\n\n\n");
                                P.consultar();
                                System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                SW = T.nextInt();  
                                break;
                            case 3:
                                System.out.println("\n\n\n\n\n\n\n\n\n");
                                System.out.println("********************************************************");
                                System.out.println("*****       Menu  *MODIFICAR* para Productos       *****");
                                System.out.println("********************************************************");
                                if(P.comprobar()==0){
                                    System.out.println("");
                                    System.out.println("********************************************************");
                                    System.out.println("*           !..[ERROR] NO  existe registro             *");
                                    System.out.println("*             Debe Adicionar Registro.....             *");
                                    System.out.println("********************************************************");
                                    System.out.println("");
                                    System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                    SW = T.nextInt();      
                                 }
                                else{
                                     SW=2;
                                    System.out.println("Ingrese el Codigo Producto..... "); 
                                    T.nextLine();
                                    P.setID_CODIGO(T.nextInt());
                                    T.nextLine();
                                    if(P.comprobar_det()==1){
                                            P.consultar_det();
                                            System.out.println("****      INGRESE NUEVOS DATOS ****"); 
                                            System.out.println("Referencia....... ");
                                            P.setREFERENCIA(T.nextLine()); 
                                            System.out.println("Descripcion...... ");
                                            P.setDESCRIPCION(T.nextLine());
                                            System.out.println("Unidad........... ");
                                            P.setUNIDAD(T.nextLine());
                                            System.out.println("Precio Venta..... ");
                                            P.setPRECIO(T.nextInt());
                                            T.nextLine();
                                            System.out.println("Costo............ ");
                                            P.setCOSTO(T.nextInt());
                                            T.nextLine();
                                            System.out.println("Existencia Actual.");
                                            P.setEXISTENCIA(T.nextInt());
                                            T.nextLine();
                                            System.out.println("Minimo........... ");
                                            P.setMINIMO(T.nextInt());
                                            T.nextLine();
                                            System.out.println("Maximo........... ");
                                            P.setMAXIMO(T.nextInt());
                                            T.nextLine();
                                            P.actualizar();
                                    }
                                    else{
                                         System.out.println("*******************************************");
                                         System.out.println("**** CODIGO DE REGISTRO NO EXISTE....******");
                                         System.out.println("*******************************************");
                                         System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                         SW = T.nextInt();   
                                    }
                                }
                                break;
                                          
                            case 4:
                                System.out.println("\n\n\n\n\n\n\n\n\n");
                                System.out.println("********************************************************");
                                System.out.println("*****       Menu  *ELIMINAR* para Terceros       ******");
                                System.out.println("********************************************************");                               
                                if(P.comprobar()==0){
                                    System.out.println("");
                                    System.out.println("********************************************************");
                                    System.out.println("*           !..[ERROR] NO  existe registro             *");
                                    System.out.println("*             Debe Adicionar Registro.....             *");
                                    System.out.println("********************************************************");
                                    System.out.println("");
                                    System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                    SW = T.nextInt();      
                                 }
                                else{
                                    SW=2;
                                    System.out.println("Codigo Producto..... "); 
                                    T.nextLine();
                                    P.setID_CODIGO(T.nextInt());
                                    T.nextLine();
                                    if(P.comprobar_det()==1){
                                           P.consultar_det();
                                           while (SW==2){
                                                  System.out.println("**** DESEA ELIMINAR EL REGISTRO PULSE [1]Si ó [0]No....");
                                                  SW = T.nextInt();
                                                  switch(SW)
                                                           {
                                                            case 1:
                                                                P.borrar();
                                                                SW=1;
                                                                break;
                                                            case 0:
                                                                SW=0;
                                                                break;
                                                            default:
                                                                SW=2;
                                                                System.out.print("");
                                                                System.out.println("**** OPCION NO VALIDA INTENTE DE NUEVO....");
                                                                System.out.print("");
                                                            }
                                                 }
                                           
                                           }
                                    else{
                                         System.out.println("*******************************************");
                                         System.out.println("**** CODIGO DE REGISTRO NO EXISTE....******");
                                         System.out.println("*******************************************");
                                         System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                         SW = T.nextInt();   
                                    }
                                    
                                }
                                break;
                            default:
                                M2=5;
                         }
                   }
                    break;
/****FIN MENU PARA EL CRUD  TABLA PRODUCTOS**********************************************************/                                                                                                            




                   case 4:
                       
                       
                               
                       
/****INICIO PARA EL CRUD  TABLA TERCEROS**********************************************************/    
                       while(M2!=5){    
                        System.out.println("\n\n\n\n\n\n\n\n\n");
                        System.out.println("********************************************************");
                        System.out.println("***          Bienvenido al Programa Tendero          ***");
                        System.out.println("********************************************************");
                        System.out.println("******    Menu de Actualización de Terceros      ******");
                        System.out.println("********************************************************");
                        System.out.println("*              1.Adicionar                             *");
                        System.out.println("*              2.Consultar                             *");
                        System.out.println("*              3.Modificar                             *");
                        System.out.println("*              4.Eliminar                              *");
                        System.out.println("*              5.Regresar al menu anterior             *");
                        System.out.println("********************************************************");
                        System.out.println("        Ingrese un Número del [1] al [5]  ");
                        M2 = T.nextInt();
                        System.out.println(" ");
                        switch(M2)
                            {
                            case 1 :
                                System.out.println("\n\n\n\n\n\n\n\n\n");
                                System.out.println("********************************************************");
                                System.out.println("*****       Menu  *ADICIONAR* para Terceros       ******");
                                System.out.println("********************************************************");                                                                
                                System.out.println("Codigo Tercero..... "); 
                                T.nextLine();
                                C.setID_TERCERO(T.nextInt());
                                T.nextLine();
                                if(C.comprobar_det()==1){
                                    System.out.println("");
                                    System.out.println("********************************************************");
                                    System.out.println("*           !..[ERROR] Registro ya existe              *");
                                    System.out.println("*             Debe Usar otro Código de Tercero        *");
                                    System.out.println("********************************************************");
                                    System.out.println("");
                                    System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                    SW = T.nextInt();   
                                }
                                else {
                                
                                    System.out.println("Identificación.... ");
                                    C.setNUMERO(T.nextLine()); 
                                    System.out.println("Nombre............ ");
                                    C.setNOMBRE(T.nextLine());
                                    System.out.println("Apellido.......... ");
                                    C.setAPELLIDO(T.nextLine());
                                    System.out.println("Teléfonos......... ");
                                    C.setTELEFONO(T.nextLine());
                                    System.out.println("Dirección......... ");
                                    C.setDIRECCION(T.nextLine());
                                    System.out.println("Correo Electrónico.");
                                    C.setEMAIL(T.nextLine());
                                    System.out.println("Ciudad............ ");
                                    C.setCIUDAD(T.nextLine());
                                    System.out.println("Es proveedor[S/N]. ");
                                    C.setTIPO_PROV(T.nextLine());
                                    System.out.println("Es Cliente [S/N].. ");
                                    C.setTIPO_CLI(T.nextLine());
                                    System.out.println("Es Usuario [S/N].. ");
                                    C.setTIPO_USER(T.nextLine());
                                    System.out.println("Nivel Autorización.");                                                           
                                    if(C.getTIPO_USER().equals("S")){
                                        C.setPERM_USER(T.nextInt());
                                    }
                                    else{
                                        C.setPERM_USER(0); 
                                    }
                                    C.guardar();
                                }
                                break;
                            case 2 :
                                System.out.println("********************************************************");
                                System.out.println("*****       Menu  *CONSULTAR* para Terceros       ******");
                                System.out.println("********************************************************");
                                System.out.println("\n\n\n\n\n\n\n\n\n");
                                C.consultar();
                                System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                SW = T.nextInt();  
                                break;
                            case 3:
                                System.out.println("\n\n\n\n\n\n\n\n\n");
                                System.out.println("********************************************************");
                                System.out.println("*****       Menu  *MODIFICAR* para Terceros       ******");
                                System.out.println("********************************************************");
                                if(C.comprobar()==0){
                                    System.out.println("");
                                    System.out.println("********************************************************");
                                    System.out.println("*           !..[ERROR] NO  existe registro             *");
                                    System.out.println("*             Debe Adicionar Registro.....             *");
                                    System.out.println("********************************************************");
                                    System.out.println("");
                                    System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                    SW = T.nextInt();      
                                 }
                                else{
                                     SW=2;
                                    System.out.println("Codigo Tercero..... "); 
                                    T.nextLine();
                                    C.setID_TERCERO(T.nextInt());
                                    T.nextLine();
                                    if(C.comprobar_det()==1){
                                           C.consultar_det();
                                           System.out.println("****      INGRESE NUEVOS DATOS ****"); 
                                           System.out.println("Identificación.... ");
                                           C.setNUMERO(T.nextLine()); 
                                           System.out.println("Nombre............ ");
                                           C.setNOMBRE(T.nextLine());
                                           System.out.println("Apellido.......... ");
                                           C.setAPELLIDO(T.nextLine());
                                           System.out.println("Teléfonos......... ");
                                           C.setTELEFONO(T.nextLine());
                                           System.out.println("Dirección......... ");
                                           C.setDIRECCION(T.nextLine());
                                           System.out.println("Correo Electrónico.");
                                           C.setEMAIL(T.nextLine());
                                           System.out.println("Ciudad............ ");
                                           C.setCIUDAD(T.nextLine());
                                           System.out.println("Es proveedor[S/N]. ");
                                           C.setTIPO_PROV(T.nextLine());
                                           System.out.println("Es Cliente [S/N].. ");
                                           C.setTIPO_CLI(T.nextLine());
                                           System.out.println("Es Usuario [S/N].. ");
                                           C.setTIPO_USER(T.nextLine());
                                           System.out.println("Nivel Autorización.");
                                           if(C.getTIPO_USER().equals("S")){
                                                 C.setPERM_USER(T.nextInt());
                                            }
                                             else{
                                                C.setPERM_USER(0); 
                                            }
                                           C.actualizar();
                                    }
                                   else{
                                         System.out.println("*******************************************");
                                         System.out.println("**** CODIGO DE REGISTRO NO EXISTE....******");
                                         System.out.println("*******************************************");
                                         System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                         SW = T.nextInt();
                                    }
                                }
                                break;       
                            case 4:
                                System.out.println("\n\n\n\n\n\n\n\n\n");
                                System.out.println("********************************************************");
                                System.out.println("*****       Menu  *ELIMINAR* para Terceros       ******");
                                System.out.println("********************************************************");                               
                                if(C.comprobar()==0){
                                    System.out.println("");
                                    System.out.println("********************************************************");
                                    System.out.println("*           !..[ERROR] NO  existe registro             *");
                                    System.out.println("*             Debe Adicionar Registro.....             *");
                                    System.out.println("********************************************************");
                                    System.out.println("");
                                    System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                    SW = T.nextInt();      
                                 }
                                else{
                                    SW=2;
                                    System.out.println("Codigo Tercero..... "); 
                                    T.nextLine();
                                    C.setID_TERCERO(T.nextInt());
                                    T.nextLine();
                                    if(C.comprobar_det()==1){
                                           C.consultar_det();
                                           while (SW==2){
                                                  System.out.println("**** DESEA ELIMINAR EL REGISTRO PULSE [1]Si ó [0]No....");
                                                  SW = T.nextInt();
                                                  switch(SW)
                                                           {
                                                            case 1:
                                                                C.borrar();
                                                                SW=1;
                                                                break;
                                                            case 0:
                                                                SW=0;
                                                                break;
                                                            default:
                                                                SW=2;
                                                                System.out.print("");
                                                                System.out.println("**** OPCION NO VALIDA INTENTE DE NUEVO....");
                                                                System.out.print("");
                                                            }
                                                 }
                                           
                                           }
                                    else{
                                         System.out.println("*******************************************");
                                         System.out.println("**** CODIGO DE REGISTRO NO EXISTE....******");
                                         System.out.println("*******************************************");
                                         System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                         SW = T.nextInt();   
                                    }
                                    
                                }
                                break;
                            default:
                                M2=5;
                         }
                   }
                    break;
/****FIN MENU PARA EL CRUD  TABLA TERCEROS**********************************************************/                                                                                     




                   case 5:

                       
                       
                       
                       
                       System.out.println("********************************************************");
                         System.out.println("**  Opcoón de Reportes Especiales en [CONSTRUCCION].. **");
                         System.out.println("********************************************************");                        
                         System.out.println(" Pulse [0] y [ENTER] para continuar..... ");
                         M2 = T.nextInt();
                         break;  
                         
                         
                   case 6:
                       
                       
                       
                       
/****INICIO MENU PARA EL CRUD  TABLA DATOS BASICOS**********************************************************/
                    while(M2!=5){    
                        System.out.println("\n\n\n\n\n\n\n\n\n");
                        System.out.println("********************************************************");
                        System.out.println("***          Bienvenido al Programa Tendero          ***");
                        System.out.println("********************************************************");
                        System.out.println("*****    Menu de Actualización Datos Básicos      ******");
                        System.out.println("********************************************************");
                        System.out.println("*              1.Adicionar                             *");
                        System.out.println("*              2.Consultar                             *");
                        System.out.println("*              3.Modificar                             *");
                        System.out.println("*              4.Eliminar                              *");
                        System.out.println("*              5.Regresar al menu anterior             *");
                        System.out.println("********************************************************");
                        System.out.println("        Ingrese un Número del [1] al [5]  ");
                        M2 = T.nextInt();
                        System.out.println(" ");
                        switch(M2)
                            {
                            case 1 :
                                System.out.println("\n\n\n\n\n\n\n\n\n");
                                System.out.println("********************************************************");
                                System.out.println("*****       Menu  *ADICIONAR* Datos Básicos       ******");
                                System.out.println("********************************************************");                                
                                System.out.println("Identificación............... "); 
                                T.nextLine();
                                a.setID(T.nextLine());
                                //T.nextLine();
                                if(a.comprobar_det()==1){
                                    System.out.println("");
                                    System.out.println("********************************************************");
                                    System.out.println("*           !..[ERROR] Registro ya existe              *");
                                    System.out.println("*             Debe Usar otro Código de producto        *");
                                    System.out.println("********************************************************");
                                    System.out.println("");
                                    System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                    SW = T.nextInt();   
                                }
                                else {
                                      
                                     System.out.println("Nombre de la tienda.......... ");
                                      a.setNOM_TIENDA(T.nextLine());
                                     System.out.println("Identificación Representante. ");
                                      a.setNUM_IDENTI(T.nextLine());
                                     System.out.println("Nombre del Representante..... ");
                                      a.setNOM_REPRE(T.nextLine());
                                     System.out.println("Telefonos de la Tienda....... ");
                                      a.setTELEFONO(T.nextLine());
                                     a.guardar();
                                }
                                break;
                            case 2 :
                                System.out.println("********************************************************");
                                System.out.println("*****       Menu  *CONSULTAR* Datos Básicos       ******");
                                System.out.println("********************************************************");                                
                                System.out.println("\n\n\n\n\n\n\n\n\n");
                                a.consultar();
                                System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                SW = T.nextInt();  
                                break;
                            case 3:
                                System.out.println("********************************************************");
                                System.out.println("*****       Menu  *MODIFICAR* Datos Básicos       ******");
                                System.out.println("********************************************************");                                
                                System.out.println("\n\n\n\n\n\n\n\n\n");
                                System.out.println("Ingrese Identificación............... "); 
                                T.nextLine();
                                a.setID(T.nextLine());
                                if(a.comprobar_det()==0){
                                    System.out.println("");
                                    System.out.println("********************************************************");
                                    System.out.println("*           !..[ERROR] NO  existe registro             *");
                                    System.out.println("*             Debe Adicionar Registro.....             *");
                                    System.out.println("********************************************************");
                                    System.out.println("");
                                    System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                    SW = T.nextInt();
                                }
                                else{
                                    a.consultar_det();
                                     System.out.println("****      INGRESE NUEVOS DATOS ****"); 
                                     System.out.println("Nombre de la tienda.......... ");
                                     a.setNOM_TIENDA(T.nextLine());
                                     System.out.println("Identificación Representante. ");
                                      a.setNUM_IDENTI(T.nextLine());
                                     System.out.println("Nombre del Representante..... ");
                                      a.setNOM_REPRE(T.nextLine());
                                     System.out.println("Telefonos de la Tienda....... ");
                                      a.setTELEFONO(T.nextLine());
                                     a.actualizar();
                                  }
                                  break;       
                            case 4:
                                System.out.println("********************************************************");
                                System.out.println("*****       Menu  *ELIMINAR* Datos Básicos       *******");
                                System.out.println("********************************************************");                                
                                System.out.println("\n\n\n\n\n\n\n\n\n");
                                System.out.println("Ingrese Identificación............... "); 
                                T.nextLine();
                                a.setID(T.nextLine());
                                if(a.comprobar_det()==0){
                                    System.out.println("");
                                    System.out.println("********************************************************");
                                    System.out.println("*           !..[ERROR] NO  existe registro             *");
                                    System.out.println("*             Debe Adicionar Registro.....             *");
                                    System.out.println("********************************************************");
                                    System.out.println("");
                                    System.out.println("Pulse [0] y [ENTER] para continuar.....");
                                    SW = T.nextInt();      
                                 }
                                else{
                                    SW=2;
                                    a.consultar_det();
                                    while (SW==2){
                                        System.out.println("**** DESEA ELIMINAR EL REGISTRO PULSE [1]Si ó [0]No....");
                                        SW = T.nextInt();
                                        switch(SW)
                                                  {
                                                   case 1:
                                                       a.borrar();
                                                       SW=1;
                                                       break;
                                                   case 0:
                                                       SW=0;
                                                       break;
                                                   default:
                                                       SW=2;
                                                       System.out.print("");
                                                       System.out.print("**** OPCION NO VALIDA INTENTE DE NUEVO....");
                                                       System.out.print("");
                                                   }
                                        }
                                }                               
                                
                                break;
                            default:
                                M2=5;
                         }
                       
                    }
                    break;
/****FIN MENU PARA EL CRUD  TABLA DATOS BASICOS**********************************************************/                    
             default:
                     M1=7;
            }
          
            System.out.println("\n\n\n\n\n\n\n\n\n");
            System.out.println("");
            
        }
       
            System.out.println("\n\n\n\n\n\n\n\n\n");
            System.out.println("");
            System.out.println("********************************************************");
            System.out.println("*         GRACIAS POR UTILIZAR NUESTRO SOFTWARE...      *");
            System.out.println("*                  **    MINTIC  **                    *");
            System.out.println("********************************************************");
            System.out.println("");
       
      
    }

   public void MENU6(){
   }
   
}



