
package Persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class Conexion {
    
    // Atributos
    String url="jdbc:mysql://localhost:3306/tienda";
    String user="root";
    String password= "clave";

    // Constructor
    public Conexion(){
        // jdbc:mysql protocolo de conexión a la DB 
        // luego viene el dominio - localhost -> :3306 es el puerto    
    }
    
    // Métodos
    public Connection iniciarConexion() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection  connection=DriverManager.getConnection(this.url,this.user,this.password);
        return connection;         
    }
    
}








/*


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;


 *
 * @author ADMIN
 */
/*
public class Conexion {
// Atributos
    String url="jdbc:mysql://localhost:3306/tienda";
    String user="root";
    String password= "720509";
    Connection Con;

        public Conexion() throws ClassNotFoundException, SQLException{
        try{
            Class.forName("com.mysql.jdbc.Driver");
           Con = DriverManager.getConnection(this.url,this.user,this.password);
          }catch(SQLException e){System.err.println("Error" + e);}
       }
        public void mostrarconexion(){
            System.out.println("Conexion exitosa: " +this.url );
        }
    public static void   main(String[] args) throws ClassNotFoundException, SQLException{
        Conexion cn=new Conexion();
        cn.mostrarconexion();
        Statement st = null ;
        Resultset rs;
        try{
           st=cn.Con.createStatement();
           System.out.println(st);
           rs= st.execute("select * from inicial");
           
           st.close();
        }catch(SQLException e){System.err.println("Error" + e);}
        
        

    }
}

*/